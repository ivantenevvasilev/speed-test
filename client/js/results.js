function getResults() {
    let username = localStorage.getItem("username");
    // make sure user is same as cookie on server side.

    let path = API_BASE_URL + ENDPOINTS.RESULTS;
    if (username) {
        path = path + "?username=" + username;
    }

    get(path, (data) => {
        let table = document.createElement("table");
        let header = document.createElement("thead");
        
        let speedHeader = document.createElement("tr");
        

        let testNumberCell = document.createElement("th");
        let testNumberCellText = document.createTextNode("#");
        testNumberCell.appendChild(testNumberCellText);
        speedHeader.appendChild(testNumberCell);

        let idCell = document.createElement("th");
        let idCellText = document.createTextNode("ID");
        idCell.appendChild(idCellText);
        speedHeader.appendChild(idCell);


        let uploadSpeedCell = document.createElement("th");
        let uploadSpeedCellText = document.createTextNode("Upload Speed");
        uploadSpeedCell.appendChild(uploadSpeedCellText);
        speedHeader.appendChild(uploadSpeedCell);

        let downloadSpeedCell = document.createElement("th");
        let downloadSpeedCellText = document.createTextNode("Download Speed");
        downloadSpeedCell.appendChild(downloadSpeedCellText);
        speedHeader.appendChild(downloadSpeedCell);

        let pingCell = document.createElement("th");
        let pingCellText = document.createTextNode("Ping");
        pingCell.appendChild(pingCellText);
        speedHeader.appendChild(pingCell);

        let jitterCell = document.createElement("th");
        let jitterCellText = document.createTextNode("Jitter");
        jitterCell.appendChild(jitterCellText);
        speedHeader.appendChild(jitterCell);

        let durationCell = document.createElement("th");
        let durationCellText = document.createTextNode("Duration");
        durationCell.appendChild(durationCellText);
        speedHeader.appendChild(durationCell);

        header.appendChild(speedHeader);
        table.appendChild(header);

        let tbody = document.createElement("tbody");

        let i =  1;
        for (let rowData of data["data"]) {
            let row = document.createElement("tr");
            
            let testNumberCell = document.createElement("td");
            let testNumberCellText = document.createTextNode(`${i}`);
            testNumberCell.appendChild(testNumberCellText);
            row.appendChild(testNumberCell);
    
            let idCell = document.createElement("td");
            let link = document.createElement("a");
            link.href = `${API_BASE_URL}${ENDPOINTS.CSV}?id=${rowData["id"]}`;
            link.download = `test-${rowData["id"]}.csv`;
            let linkTextNode = document.createTextNode(`${rowData["id"]}`);
            link.appendChild(linkTextNode);
            idCell.appendChild(link);
            row.append(idCell);
            let uploadSpeedCell = document.createElement("td");
            let uploadSpeed = speedFromBytes(+rowData["averageUploadSpeed"]);
            let uploadSpeedCellText = document.createTextNode(`${uploadSpeed.speed.toFixed(2)} ${uploadSpeed.metric}`);
            uploadSpeedCell.appendChild(uploadSpeedCellText);
            row.appendChild(uploadSpeedCell);
    
            let downloadSpeedCell = document.createElement("td");
            let downloadSpeed = speedFromBytes(+rowData["averageDownloadSpeed"]);
            let downloadSpeedCellText = document.createTextNode(`${downloadSpeed.speed.toFixed(2)} ${downloadSpeed.metric}`);
            downloadSpeedCell.appendChild(downloadSpeedCellText);
            row.appendChild(downloadSpeedCell);
    
            let pingCell = document.createElement("td");
            let pingCellText = document.createTextNode(`${+rowData["latency"]}`);
            pingCell.appendChild(pingCellText);
            row.appendChild(pingCell);
    
            let jitterCell = document.createElement("td");
            let jitterCellText = document.createTextNode(`${+rowData["jitter"]}`);
            jitterCell.appendChild(jitterCellText);
            row.appendChild(jitterCell);
    
            let durationCell = document.createElement("td");
            let startTime = new Date(rowData["testStart"]).getTime();
            let endTime = new Date(rowData["testEnd"]).getTime();
            let duration = (endTime - startTime) / 1000;

            let durationCellText = document.createTextNode(`${duration}s`);
            durationCell.appendChild(durationCellText);
            row.appendChild(durationCell);

            tbody.appendChild(row);
            i++;
        }
        table.appendChild(tbody);

        let target = document.getElementById("table-div");
        target.appendChild(table);
    });
}