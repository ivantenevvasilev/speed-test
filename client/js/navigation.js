function setNavigation() {
    let guestNav = document.getElementById("guest-nav");
    let userNav = document.getElementById("user-nav");

    if (localStorage.getItem("username") !== null) {
        guestNav.style.display = "none";
        userNav.style.display = "block";

        let welcomeText = document.getElementById("welcome");
        welcomeText.innerText = `Welcome, ${localStorage.getItem("username")}`;
    } else {
        guestNav.style.display = "block";
        userNav.style.display = "none";
    }
}