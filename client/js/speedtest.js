function startTest() {
    let startTime = getSQLTime(new Date());
    let button = document.getElementById("test-button");
    button.disabled = true;
    const xhr = new XMLHttpRequest();
    let data = [];
    xhr.open("GET", ENDPOINTS.TESTFILE, true);
    xhr.responseType = "arraybuffer";
    let last;

    let minPing;
    let maxPing;
    
    xhr.addEventListener("loadstart", e => {
        last = {
            timeStamp: e.timeStamp,
            total: e.total,
            loaded: e.loaded
        };

        maxPing = last.timeStamp / 1000;
        minPing = last.timeStamp / 1000;
        let downloadStatusElement = document.getElementById("download-status");
        downloadStatusElement.innerText = "Running";
    });
    let speedSum = 0;
    let pingSum = 0;
    let count = 1;
  
    
    xhr.addEventListener("progress", e => {
        let current = {
            timeStamp: e.timeStamp,
            total: e.total,
            loaded: e.loaded
        };
        let latency = (current.timeStamp - last.timeStamp) / 1000;
        let diff = {
            transferredBytes: current.loaded - last.loaded,
            latencyData: latency
        };
        
        pingSum += diff.latencyData;

        speedSum += diff.transferredBytes / latency;
        ++count;

        minPing = Math.min(minPing, diff.latencyData);
        maxPing = Math.max(maxPing, diff.latencyData);

        updateMetrics(null, speedSum / count, pingSum / count, maxPing - minPing);


        data.push(diff);
        last = current;
    })
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            file = xhr.response;
            let downloadStatusElement = document.getElementById("download-status");
            downloadStatusElement.innerText = "Finished";
            startUploadTest(file, startTime, speedSum / count, data);
        }
    }

    xhr.send();
}

function startUploadTest(file, startTime, averageDownload, downloadTicks) {
    const form = new FormData();
    
    const blob = form.append("blob", new Blob([file], {type: 'application/octet-stream'}));

    form.append('blob', blob);
    

    const xhr = new XMLHttpRequest();
    let data = [];
    xhr.open("POST", API_BASE_URL + ENDPOINTS.UPLOADTEST);
    let last;

    let minPing;
    let maxPing;
    
    xhr.upload.addEventListener("loadstart", e => {
        let uploadStatusElement = document.getElementById("upload-status");
        uploadStatusElement.innerText = "Running";
        last = {
            timeStamp: e.timeStamp,
            total: e.total,
            loaded: e.loaded
        };
        //ping = last.timeStamp / 1000;
        maxPing = last.timeStamp / 1000;
        minPing = last.timeStamp / 1000;
    });
    let speedSum = 0;
    let pingSum = 0;
    let count = 1;
  
    
    xhr.upload.addEventListener("progress", e => {
        let current = {
            timeStamp: e.timeStamp,
            total: e.total,
            loaded: e.loaded
        };
        let latency = (current.timeStamp - last.timeStamp) / 1000;
        let diff = {
            transferredBytes: current.loaded - last.loaded,
            latencyData: latency
        };
        
        pingSum += diff.latencyData;

        speedSum += diff.transferredBytes / latency;
        ++count;

        minPing = Math.min(minPing, diff.latencyData);
        maxPing = Math.max(maxPing, diff.latencyData);

        updateMetrics(speedSum / count, null, pingSum / count, maxPing - minPing);


        data.push(diff);
        last = current;
    })
    xhr.upload.addEventListener("load", _ => {
        let endTime = getSQLTime(new Date());

        let uploadStatusElement = document.getElementById("upload-status");
        uploadStatusElement.innerText = "Finished";
        let button = document.getElementById("test-button");
        button.disabled = false;
        downloadTicks = downloadTicks.map(d => {
            return {
                "bytes": d.transferredBytes,
                "latency": d.latencyData,
                "isUpload": 0
            }
        });
        uploadTicks = data.map(d => {
            return {
                "bytes": d.transferredBytes,
                "latency": d.latencyData,
                "isUpload": 1
            }
        });
        ticks = downloadTicks.concat(uploadTicks);
        post(API_BASE_URL + ENDPOINTS.SUBMITTEST, {
            "uploadSpeed": speedSum / count,
            "downloadSpeed": averageDownload,
            "jitter": maxPing - minPing,
            "latency": pingSum / count,
            "startTime": startTime,
            "endTime": endTime,
            "ticks": ticks
        }, (result) => {
            console.log(result);
        }, (error) => {
            console.log(error);
        });
    });
    xhr.setRequestHeader("Content-type","multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2));
    xhr.send(form);
}

function updateMetrics(averageUploadSpeed, averageDownloadSpeed, ping, jitter) {
    let jitterElement = document.getElementById("jitter");
    let pingElement = document.getElementById("ping");
    let uploadMetricElement = document.getElementById("upload-speed");
    let downloadMetricElement = document.getElementById("download-speed");

    jitterElement.innerText = `${jitter.toFixed(2)}ms`;
    pingElement.innerText = `${ping.toFixed(2)}ms`;
    if (averageUploadSpeed !== null) {
        let speed = speedFromBytes(averageUploadSpeed);
        uploadMetricElement.innerText = `${speed.speed.toFixed(2)} ${speed.metric}`;
        uploadMetricElement.title = `${speed.speed} ${speed.metric}`;
        
    }
    if (averageDownloadSpeed !== null) {
        let speed = speedFromBytes(averageDownloadSpeed);
        downloadMetricElement.innerText = `${speed.speed.toFixed(2)} ${speed.metric}`;
        downloadMetricElement.title = `${speed.speed} ${speed.metric}`;
    }

}
