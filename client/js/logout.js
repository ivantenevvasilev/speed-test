function logout() {
    delete_cookie("token");
    sessionStorage.clear();
    localStorage.clear();
    setNavigation();
    get(API_BASE_URL + ENDPOINTS.LOGOUT, (_) => {
        window.location.href = "index.html";
    }, (error) => {
        window.location.href = "index.html";
        console.log(error);
    });

}