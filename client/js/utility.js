function post(url, data, success, failure) {
    fetch(url, 
        { 
            method: "POST", 
            headers: {
                'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body: `data=${JSON.stringify(data)}`,
        })
        .then(response => response.json())
        .then(response => {
            if (response["success"] === true) {
                success(response);
            } else {
                failure(response["error"]);
            }
        })
        .catch(error => failure(error));
}

function get(url, success, failure) {
    fetch(url, 
        { 
            method: "GET"
        })
        .then(response => response.json())
        .then(response => {
            if (response["success"] === true) {
                success(response);
            } else {
                failure(response["error"]);
            }
        });
}

function speedFromBytes(bytes) {
    let speed = bytes * 8;
    let extension = "Bps";
    if (speed > 1024) {
        speed /= 1024;
        extension = "Kbps";
    }
    if (speed > 1024) {
        speed /= 1024;
        extension = "Mbps";
    }
    if (speed > 1024) {
        speed /= 1024;
        extension = "Gpbs";
    }
    return {
        speed: speed,
        metric: extension
    };
}

function getSQLTime(date) {
    var pad = function(num) { return ('00'+num).slice(-2) };
    return date.getUTCFullYear()         + '-' +
        pad(date.getUTCMonth() + 1)  + '-' +
        pad(date.getUTCDate())       + ' ' +
        pad(date.getUTCHours())      + ':' +
        pad(date.getUTCMinutes())    + ':' +
        pad(date.getUTCSeconds());
}

function delete_cookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
