function generate_random_data(size) {
    return new Blob([new ArrayBuffer(size)], {type: 'application/octet-stream'});
};
