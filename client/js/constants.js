const API_BASE_URL = "http://skeleton.tristram.com/web-project/api/api.php/";
const ENDPOINTS = {
    "REGISTER": "register",
    "LOGIN": "login",
    "UPLOADTEST": "upload",
    "SUBMITTEST": "submit_test",
    "RESULTS": "results",
    "LOGOUT": "logout",
    "CSV": "test",
    "TESTFILE": "http://skeleton.tristram.com/web-project/api/test_files/100mb-test"
};