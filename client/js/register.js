function register(e) {
    e.preventDefault();

    let username = document.getElementById("username-field").value;
    let password = document.getElementById("password-field").value;
    let onSuccess = (response) => {
        localStorage.setItem("username", username);

        window.location.href = "index.html";
    };

    let onError = (errorText) => {
        let errorSpan = document.getElementById("error");
        errorSpan.innerText = errorText;
        errorSpan.style.display = "block";
    }
    
    post(API_BASE_URL + ENDPOINTS.REGISTER,
        {
            "username": username,
            "password": password
        }, onSuccess, onError);
}

