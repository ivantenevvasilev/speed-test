# Web Project
Project written for the Web Programming course in FMI

## Configuration

### Server - [config.ini](config.ini)
- `hostname` - specifies the IP/DNS entry for the database
- `port` - specifies the database listening port (should be 3306 by default)
- `database_name` - specifies the database that will be used for this project. It must exist beforehand, if you specify name, `speedtest` you have to have run the following query beforehand

```sql
CREATE DATABASE speedtest;
```

- `user` - the user that our server will be using, it needs permissions for reading, writing data and creating tables.
- `password` - the password for the corresponding user

### Client - [constants.js](api/js/constants.js)
The most important thing is the `API_BASE_URL` - specifies the location to `api/api.php` on the server by default. The only other configurable thing is `ENDPOINTS.TESTFILE` - specifies URI to the file that will be used for testing the speed.

### Script for generating random files on Linux

```bash
#! /bin/bash

# Generating random files for upload/download tests.

head -c 104857600 </dev/urandom >100mb-test
head -c 1073741824 </dev/urandom >1gb-test
head -c 2147483648 </dev/urandom >2gb-test
head -c 10737418240 </dev/urandom >10gb-test
```
