CREATE DATABASE speedtest1;
USE speedtest1;

CREATE TABLE Users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(128) UNIQUE,
    password VARCHAR(256)
);

CREATE TABLE Tokens (
    id INT AUTO_INCREMENT PRIMARY KEY,
    token VARCHAR(500) NOT NULL,
    userId INT NOT NULL,
    expires VARCHAR(500) NOT NULL,
    FOREIGN KEY (userId) REFERENCES Users(id)
);

CREATE TABLE Devices (
    id INT AUTO_INCREMENT PRIMARY KEY,
    ipAddress VARCHAR(45),
    userAgent VARCHAR(1000),
    userId INT,
    FOREIGN KEY (userId) REFERENCES Users(id)
);

CREATE TABLE Tests (
    id INT AUTO_INCREMENT PRIMARY KEY,
    testStart TIMESTAMP,
    testEnd TIMESTAMP,
    averageUploadSpeed DOUBLE,
    averageDownloadSpeed DOUBLE,
    latency DOUBLE,
    jitter DOUBLE,
    deviceId INT,
    FOREIGN KEY (deviceId) REFERENCES Devices(id)
);

CREATE TABLE Ticks (
    id INT AUTO_INCREMENT PRIMARY KEY,
    testId INT NOT NULL,
    bytes INT NOT NULL,
    latency DOUBLE NOT NULL,
    isUpload BOOLEAN NOT NULL,
    FOREIGN KEY (testId) REFERENCES Tests(id)
);
