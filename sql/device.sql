CREATE TABLE Devices (
    id INT AUTO_INCREMENT PRIMARY KEY,
    ipAddress VARCHAR(45),
    userAgent VARCHAR(1000),
    userId INT,
    FOREIGN KEY (userId) REFERENCES Users(id)
)