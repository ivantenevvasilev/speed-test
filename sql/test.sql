CREATE TABLE Tests (
    id INT AUTO_INCREMENT PRIMARY KEY,
    testStart TIMESTAMP,
    testEnd TIMESTAMP,
    averageUploadSpeed DOUBLE,
    averageDownloadSpeed DOUBLE,
    latency DOUBLE,
    jitter DOUBLE,
    deviceId INT,
    FOREIGN KEY (deviceId) REFERENCES Devices(id)
)