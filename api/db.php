<?php
class Database {
    private $config;
    public $connection;

    public $selectUserStatement;
    public $insertUserStatement;
    public $selectUserByUsernameStatement;

    public $selectDeviceStatement;
    public $insertDeviceStatement;
    public $selectDeviceByAgentAndIp;
    public $updateDeviceUserId;

    public $insertTestStatement;
    public $selectTestsByUserId;
    public $selectTestsByDeviceId;
    
    public $selectTicksByTestId;


    public function __construct() {
        $DATABASE_INI = "database";
        $HOSTNAME_KEY = "hostname";
        $PORT_KEY = "port";
        $USER_KEY = "user";
        $PASSWORD_KEY = "password";
        $DATABASE_NAME = "database_name";
        $CONFIG_PATH = "../config.ini";
        
        $config = parse_ini_file($CONFIG_PATH, true);
        $hostname = $config[$DATABASE_INI][$HOSTNAME_KEY];
        $port = $config[$DATABASE_INI][$PORT_KEY];
        $username = $config[$DATABASE_INI][$USER_KEY];
        $password = $config[$DATABASE_INI][$PASSWORD_KEY];
        $dbname = $config[$DATABASE_INI][$DATABASE_NAME];
        $res = $this->initializeDatabase($hostname, $port, $username, $password, $dbname);
        if ($res === null) {
            $this->initializeTablesIfNonExistant();
            $this->prepareStatements();
        } else {
            echo $res;
        }
    }

    private function initializeDatabase($host, $port, $username, $password, $dbname) {
        try {
            $this->connection = new PDO("mysql:host=$host:$port;dbname=$dbname", $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

            return null;
        } catch (PDOException $e) {
            return $e;
        }
    }

    private function initializeTablesIfNonExistant() {
        $SQL_PATH = "../sql/";
        $tables = array("user", "device", "test", "token", "tick");
        foreach ($tables as $table) {
           $tableSql = file_get_contents($SQL_PATH . $table . ".sql");  
           $this->connection->exec($tableSql);
        }
    }

    private function prepareStatements() {
        $sql = "SELECT * FROM Users WHERE id=:id";
        $this->selectUserStatement = $this->connection->prepare($sql);

        $sql = "SELECT * FROM Users WHERE username=:username";
        $this->selectUserByUsernameStatement = $this->connection->prepare($sql);


        $sql = "INSERT INTO Users(username, password) VALUES (:username, :password)";
        $this->insertUserStatement = $this->connection->prepare($sql);

        $sql = "SELECT * FROM Devices WHERE id=:id";
        $this->selectDeviceStatement = $this->connection->prepare($sql);

        $sql = "SELECT * FROM Devices WHERE ipAddress=:ipAddress AND userAgent=:userAgent AND userId=:userId";
        $this->selectDeviceByAgentAndIp = $this->connection->prepare($sql);


        $sql = "INSERT INTO Devices(ipAddress, userAgent, userId) VALUES (:ipAddress, :userAgent, :userId)";
        $this->insertDeviceStatement = $this->connection->prepare($sql);

        $sql = "UPDATE Devices SET userId=:userId WHERE id=:id";
        $this->updateDeviceUserId = $this->connection->prepare($sql);

        $sql = "INSERT INTO Tokens(token, userId, expires) VALUES (:token, :userId, :expires)";
        $this->insertTokenStatement = $this->connection->prepare($sql);

        $sql = "SELECT * FROM Tokens WHERE token=:token";
        $this->selectTokenStatement = $this->connection->prepare($sql);

        $sql = "INSERT INTO Tests(testStart, testEnd, averageUploadSpeed, averageDownloadSpeed, latency, jitter, deviceId) VALUES (:testStart, :testEnd, :averageUploadSpeed, :averageDownloadSpeed, :latency, :jitter, :deviceId)";
        $this->insertTestStatement =  $this->connection->prepare($sql);

        $sql = "INSERT INTO Ticks(testId, bytes, latency, isUpload) VALUES (:testId, :bytes, :latency, :isUpload)";
        $this->insertTickStatement = $this->connection->prepare($sql);

        $sql = "SELECT t.id, t.testStart, t.testEnd, t.averageUploadSpeed, t.averageDownloadSpeed, t.latency, t.jitter FROM Tests AS t JOIN Devices AS d ON d.id= t.deviceId WHERE d.userId = :userId";
        $this->selectTestsByUserId = $this->connection->prepare($sql);
        $sql = "SELECT * FROM Tests WHERE deviceId = :deviceId";
        $this->selectTestsByDeviceId = $this->connection->prepare($sql);

        $sql = "SELECT * FROM Ticks WHERE testId = :testId";
        $this->selectTicksByTestId = $this->connection->prepare($sql);
    }
}

?>