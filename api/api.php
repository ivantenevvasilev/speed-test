<?php
    include_once 'sanitize.php';
    include_once 'User.php';
    include_once 'Device.php';
    include_once 'Token.php';
    include_once 'Test.php';
    include_once 'db.php';
    
    require 'vendor/autoload.php';

    use Aws\S3\S3Client;
    use Aws\S3\Exception\S3Exception;

    session_start();

    header("Content-type: application/json");
    $postData = null;    

    if (!isset($_SESSION["ip"]) && !isset($_SESSION["agent"])) {
        createDevice();
    }

    if (isset($_POST)) {
        if (isset($_POST["data"])) {
            $postData = json_decode($_POST["data"], true);
        } else {
            $postData = $_POST;
        }
        
        sanitize($postData);
    }

    if ($_GET) {
 //       $_GET = sanitize($_GET);
    }

    $endpoint = $_SERVER["PATH_INFO"];
    if (preg_match("/\/login$/", $endpoint)) {
        if ($_POST) {
            echo json_encode(login($postData));
        }
    }

    if (preg_match("/\/logout$/", $endpoint)) {
        echo json_encode(logout());
    }
    if (preg_match("/\/register$/", $endpoint)) {
        if ($_POST) {
            echo json_encode(register($postData));
        }
    }

    if (preg_match("/\/test$/", $endpoint)) {
        if ($_GET && isset($_GET["id"])) {
            header("Content-type: text/csv");
            return getCsv($_GET["id"]);
        }
    }

    if (preg_match("/\/results$/", $endpoint)) {
        if ($_GET) {
            $username = $_GET["username"];
            echo json_encode(get_tests($username));
        } else {
            echo json_encode(get_tests_device());
        }
    }

    if (preg_match("/\/submit_test$/", $endpoint)) {

        if ($_POST) {
            echo json_encode(upload($postData));
        }
    }
    function createToken($userId) {
        $token = bin2hex(random_bytes(8));
        $expires = time() + 60 * 60 * 24 * 30;

        setcookie("token", $token, $expires, "/");
        $token = new Token($token, $expires, $userId);
        $token->insertToken();
        $_SESSION["tokenId"] = $token->id;
    }

    
    function login($data) {
        if(isset($data['username']) && isset($data['password'])) {
            $user = new User($data['username'], $data['password']);
            $authenticated = $user->authenticate();
            if (!$authenticated["success"]) {
                return $authenticated;
            }
            $_SESSION["username"] = $authenticated["user"]["username"];
            $_SESSION["userId"] = $authenticated["user"]["id"];
            
            if (!isset($_SESSION["deviceId"])) {
                createDevice();
            }
            $device = new Device($_SESSION["ip"], $_SESSION["agent"], $_SESSION["userId"]);
            $device->id = $_SESSION["deviceId"];
            $deviceResponseObject = $device->updateUserId($user->id);


            createToken($user->id);

            return ["success" => true];
        } else {
            return ["success" => false, "error" => "Missing password or username"];
        }
    }

    function getCsv($id) {
        // TODO check cookie, and session, and that $id matches
        // a test by a device that is owned by current user
        $test = new Test();
        $test->id = $id;
        $response = $test->getTicks();
        $bucket = "speedtest-csv";
        if ($response["success"]) {
            $ticks = $response["data"];

                $s3 = new S3Client([
                    'version' => 'latest',
                    'region'  => 'us-east-1'
                ]);
            $s3->registerStreamWrapper();

            if ($s3->doesObjectExist($bucket, "$id.csv") == 0) {
                    $out = fopen("s3://$bucket/$id.csv", 'w');
                    $header = array_keys($ticks[0]);
                    fputcsv($out, $header);

                    foreach($ticks as $tick) {
                        fputcsv($out, $tick);
                    }

                fclose($out);
            }
            echo file_get_contents("s3://$bucket/$id.csv");
        }

    }

    function logout() {
        if (isset($_COOKIE["token"])) {
            setcookie("token", "", time() - 60, "/");
        }
        if (isset($_SESSION)) {
            session_unset();
            session_destroy();
        }
        return json_encode(["success" => true]);
    }

    function register($data) {
        if(isset($data['username']) && isset($data['password'])) {
            $user = new User($data['username'], $data['password']);
            $result = $user->insertUser();

            if ($result["success"]) {
                $_SESSION["userId"] = $user->id;
                $_SESSION["username"] = $data['username'];
          
                $device = new Device($_SESSION["ip"], $_SESSION["agent"], $_SESSION["userId"]);
                if (!isset($_SESSION["deviceId"])) {
                    createDevice();
                }
                $device->id = $_SESSION["deviceId"];
    
                $deviceResponseObject = $device->updateUserId($user->id);
                createToken($user->id);
            }
            

            return $result;
        } else {
            return ["success" => false];
        }
    }

    function createDevice() {
        $_SESSION["ip"] = $_SERVER["REMOTE_ADDR"];
        $_SESSION["agent"] = $_SERVER["HTTP_USER_AGENT"];
        $userId = isset($_SESSION["userId"]) ?  $_SESSION["userId"] : null;
        $device = new Device($_SESSION["ip"], $_SESSION["agent"], $userId);
        
        $deviceResponseObject = $device->insertDevice();
        $_SESSION["deviceId"] = $device->id;
    }

    function upload($data) {        
        if (!isset($_SESSION["deviceId"])) {
            createDevice();
        }

        $averageUpload = $data["uploadSpeed"];
        $averageDownload = $data["downloadSpeed"];
        $jitter = $data["jitter"];
        $latency = $data["latency"];
        $deviceId = $_SESSION["deviceId"];
        
        if (!isset($_SESSION["deviceId"])) {
            createDevice();
        }
        $device = new Device($_SESSION["ip"], $_SESSION["agent"], $_SESSION["userId"]);
        $device->id = $_SESSION["deviceId"];
        if (isset($_SESSION["userId"])) {
            $deviceResponseObject = $device->updateUserId($_SESSION["userId"]);
        }

        $ticks = $data["ticks"];
        $startTime = $data["startTime"];
        $endTime = $data["endTime"];
        $test = new Test($deviceId, $startTime, $endTime, $averageUpload, $averageDownload, $latency, $jitter, $ticks);
        return $test->insertTest();
    }

    function get_tests($username) {
        if ($_SESSION["username"] !== $username) {
            echo $_SESSION["username"];
            echo $username;
            return [ "success" => false, "error" => "Unauthorized"];
        }
        if (!isset($_SESSION["deviceId"])) {
            createDevice();
        }

        $user = new User($_SESSION["username"], "");
        $user->id = $_SESSION["userId"];

        return $user->getTests();
    }

    function get_tests_device() {
        if (!isset($_SESSION["deviceId"])) {
            createDevice();
        }
        $userId = isset($_SESSION["userId"]) ?  $_SESSION["userId"] : null;

        $device = new Device($_SESSION["ip"], $_SESSION["agent"], $userId);
        $device->id = $_SESSION["deviceId"];
        return $device->getTests();
    }
?>