<?php 
include_once 'db.php';

class Token {
    private $db;
    
    private $token;
    private $expires;
    private $userId;

    public $id;

    public function __construct($token, $expires, $userId) {
        $this->db = new Database();
        $this->token = $token;
        $this->expires = $expires;
        $this->userId = $userId;

        $this->id = null;
    }

    public function existsToken() {
        try {
            $this->db->selectTokenStatement->execute(['token' => $this->token]);
            $token = $this->db->selectTokenStatement->fetch(PDO::FETCH_ASSOC);
            if ($token) {
                $this->id = $token["id"];
            }
            return [ "success" => true, "exists" => $token];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }
    
    public function isTokenValid($userId) {
        $query = $this->existsToken();
        if ($query["success"]) {
            $token = $query["exists"];
            if($token["expires"] > time()) {
                try {
                    $userQuery = $this->db->selectUserByIdStatement.execute(["id" => $userId]);
                    $user = $userQuery->fetch(PDO::FETCH_ASSOC);
                    return [ "success" => true, "data" => ["username" => $user["username"], "id" => $user["id"] ]];
                } catch (PDOException $e) {
                    return [ "success" =>  false, "error" => $e->getMessage()];
                }
                
            } else {
                return ["success" => false, "error" => "Token expired"];
            }
        } 
        return [ "success" => false, "erorr" => "Token not found" ];
    }
    
    public function insertToken() {
        try {
            $this->db->insertTokenStatement->execute(['token' => $this->token, 'expires' => $this->expires, 'userId' => $this->userId]);
            $this->id = $this->db->connection->lastInsertId();
            return [ "success" => true ];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }



}

?>