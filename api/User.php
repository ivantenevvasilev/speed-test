<?php 
include_once 'db.php';

class User {
    private $db;
    private $username;
    private $password;
    public $id;

    public function __construct($username, $password) {
        $this->db = new Database();
        $this->username = $username;
        $this->password = $password;
        $this->id = null;
    }


    public function insertUser() {
        try {
            $exists = $this->existsUser();
            if ($exists["exists"]) {
                return [ "success" => false, "error" => "User already exists" ];
            }
            $this->db->insertUserStatement->execute(['username' => $this->username, 'password' => password_hash($this->password, PASSWORD_DEFAULT)]);
            $this->id = $this->db->connection->lastInsertId();
            return [ "success" => true ];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }

    public function authenticate() {
        $exists = $this->existsUser();
        if ($exists["exists"]) {
            $user = $exists["exists"];

            if (password_verify($this->password, $user["password"])) {
                $this->id = $user["id"];
                return ["success" => true, "user" => $user];
            } else {
                return ["success" => false, "error" => "Invalid username or password"];
            }
            
        } else {
            return ["success" => false, "error" => "Invalid username or password"];
        }
    }

    public function getTests() {
        try {
            $this->db->selectTestsByUserId->execute(['userId' => $this->id]);
            $tests = $this->db->selectTestsByUserId->fetchAll(PDO::FETCH_ASSOC);
            return [ "success" => true, "data" => $tests ];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }

    public function existsUser() {
        try {
            $this->db->selectUserByUsernameStatement->execute(['username' => $this->username]);
            return [ "success" => true, "exists" => $this->db->selectUserByUsernameStatement->fetch(PDO::FETCH_ASSOC)];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }
}
?>