<?php 
include_once 'db.php';

class Test {
    public $id;
    private $db;
    private $deviceId;
    private $startTime; 
    private $endTime;
    private $averageUpload;
    private $averageDownload;
    private $latency; 
    private $jitter; 
    private $ticks;

    public function __construct($deviceId = null, $startTime = null, $endTime = null, $averageUpload = null, $averageDownload = null, $latency = null, $jitter = null, $ticks = null) {
        $this->db = new Database();
        $this->deviceId = $deviceId;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->averageUpload = $averageUpload;
        $this->averageDownload = $averageDownload;
        $this->latency = $latency;
        $this->jitter = $jitter;
        $this->ticks = $ticks;
    }

    public function insertTest() {
        try {
            
            $this->db->insertTestStatement->execute([
                "testStart" => $this->startTime,
                "testEnd" => $this->endTime,
                "averageUploadSpeed" => $this->averageUpload,
                "averageDownloadSpeed" => $this->averageDownload,
                "latency" => $this->latency,
                "jitter" => $this->jitter,
                "deviceId" => $this->deviceId
            ]);
            $this->id = $this->db->connection->lastInsertId();
            foreach($this->ticks as $tick) {
                $this->db->insertTickStatement->execute(["testId" => $this->id, "bytes" => $tick["bytes"], "latency" => $tick["latency"], "isUpload" => $tick["isUpload"]]);
            }
            return [ "success" =>  true, "data" => $this->id];
        } catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
        
    }

    public function getTicks() {
        try {
            $this->db->selectTicksByTestId->execute(["testId" => $this->id]);
            $ticks = $this->db->selectTicksByTestId->fetchAll(PDO::FETCH_ASSOC);
            return [ "success" => true, "data" => $ticks ];
        } catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }
}
?>