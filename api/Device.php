<?php 
include_once 'db.php';

class Device {
    private $db;
    private $ip;
    private $agent;
    private $userId;
    public $id;

    public function __construct($ip, $agent, $userId) {
        $this->db = new Database();
        $this->ip = $ip;
        $this->agent = $agent;
        $this->userId = $userId;

        $this->id = null;
    }

    public function existsDevice() {
        try {
            $this->db->selectDeviceByAgentAndIp->execute(['ipAddress' => $this->ip, 'userAgent' => $this->agent, 'userId' => $this->userId]);
            $device = $this->db->selectDeviceByAgentAndIp->fetch(PDO::FETCH_ASSOC);
            if ($device) {
                $this->id = $device["id"];
            }
            return [ "success" => true, "exists" => $device];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }
    
    public function insertDevice() {
        try {
            $exists = $this->existsDevice();
            if ($exists["exists"] && $exists["exists"]["userId"] === null) {
                return [ "success" => false, "error" => "Device already exists" ];
            }
            $this->db->insertDeviceStatement->execute(['ipAddress' => $this->ip, 'userAgent' => $this->agent, 'userId' => $this->userId]);
            $this->id = $this->db->connection->lastInsertId();
            return [ "success" => true ];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }

    public function updateUserId($userId) {
        try {
            $this->db->updateDeviceUserId->execute(['userId' => $this->userId, "id" => $this->id]);
            return [ "success" => true ];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }

    public function getTests() {
        try {
            $this->db->selectTestsByDeviceId->execute(['deviceId' => $this->id]);
            $tests = $this->db->selectTestsByDeviceId->fetchAll(PDO::FETCH_ASSOC);
            return [ "success" => true, "data" => $tests ];
        }
        catch (PDOException $e) {
            return [ "success" =>  false, "error" => $e->getMessage()];
        }
    }
}

?>